# dom



## 定义

什么是 `dom`：指的是 DOM(Document Object Model——文档对象模型)是用来呈现以及与任意 HTML 或 XML文档交互的API。**可以理解为：一个标签就是一个 DOM**

什么是 `dom` 树：将 HTML 文档以树状结构直观的表现出来，整个 `dom` 树的最外层为： **`document`**。**一堆标签在一起就是 dom 树**

什么是 `dom` 对象：在 `js` 中获取到的 `html` 标签，本质上是一个对象，它就是 `dom` 对象

什么是 `ducument` 对象：是 DOM 里提供的一个 **对象**，它提供的属性和方法都是**用来访问和操作网页内容的**



## 获取 dom

1. `document.querySelector('css 选择器')`：获取到当前匹配的 **第一个** 元素
2. `document.querySelectorAll('css 选择器')`：获取到当前匹配的 **所有** 元素
3. 了解：
   1. `document.getElementById('xx')`
   2. document.getElementsByTagName('xx')
   3. `document.getElementsByClassName('xx')`



## 伪数组

定义：**长得像数组的一个对象**，内部并没有提供数组的方法

1. `querySelectorAll('xxxx')` 结果是一个伪数组
2. `arguments`  是一个伪数组



## 设置标签内容

1. `dom.innerText`：它不会渲染标签，所有内容都会作为普通文本进行处理
2. `dom.innerHTML`：它会渲染标签，里面放置的 html 标签和属性都会被渲染



## 修改元素属性

在 `html` 中，所有的属性值，都是字符串。

`dom.属性名` 进行修改：

1. `imgEle.src = xxx`
2. `imgEle.title = xxx`
2. .......

### 自定义属性

以 `data-` 开头的特殊属性

结构：`data-属性名="属性值"`

```html
<!-- data-name 就是自定义属性 -->
<!-- data-属性名="属性值" -->
<div data-name="ww">王五</div>
<div data-name="zs">张三</div>
<div data-name="ls">李四</div>
<div data-name="zl">赵六</div>
```



## 通过 style 修改元素样式

1. `dom对象.style` 可以获取元素的**行内**样式
2. 在 `style` 属性中，所有的 `xxx-xxx（比如：font-size）` 这种样式名，都会被变为《驼峰标识（`fontSize`）》
3. 通过 style 增加的样式，都会被放入到 **行内** 样式中



## 修改元素的类名

1.  `dom对象.className` 指定类名： `className` 会覆盖初始类名
2. `dom对象.classList ` 获取类名，获取到的是一个《伪数组》
   1. `add`：可以通过 `dom对象.classList.add('类名')` 来添加类名
   2. `remove`：可以通过 `dom对象.classList.remove('类名')` 来删除类名
   3. `toggle（了解）`：可以通过 `dom对象.classList.toggle('类名')` 来切换类名（有该类名就删除，没有该类名就增加）

## 为元素添加事件

`dom对象.addEventListener('事件名', function (event事件对象){})`

## 删除元素监听的事件

想要删除事件，可以使用 `removeEventListener('事件类型', 要删除的事件监听（和使用 addEventListener 时增加的事件监听在同一个内存空间）`)

# 冒泡API

## 阻止冒泡

[event.stopPropagation](https://developer.mozilla.org/zh-CN/docs/Web/API/Event/stopPropagation)



## 阻止默认行为

[event.preventDefault](https://developer.mozilla.org/zh-CN/docs/Web/API/Event/preventDefault)



## 事件委托

概念：把多个子元素的事件，统一委托给父元素。

关键属性：

1. `e.target`: 触发这个事件的最底层元素(最小的)
2. `e.target.tagName`: 分辩出当前点击的 标签名（大写）

# 定时器

## 间歇函数

`setInterval(函数, 间隔的时间（以毫秒为单位）)`： **每间隔一段时间，执行一次函数**

`setInterval` 函数会有一个返回值，为：**定时器的 `id`**

我们可以利用这个 `id` 停止定时器：`clearInterval(intervalId)`

```js
// 要求：打印一次 我爱你 之后，再停止定时器
const intervalId = setInterval(function () {
  console.log('我爱你');
	// 停止
  clearInterval(intervalId)
}, 1000)
```



## 延迟函数

`setTimeout(函数, 延迟的时间（以毫秒为单位）)`：**延迟指定时间之后，执行函数（函数仅执行一次）**

```js
setTimeout(function () {
  console.log('延迟两秒钟，做核酸啦')
}, 2000)
```

```js
// 了解：setTimeout 也可以停止，方式与 setInterval 一致
const timeoutId = setTimeout(function () {
  console.log('延迟两秒钟，做核酸啦')
}, 2000)

clearTimeout(timeoutId)
```



# window

## 概念

**代表了当前网页打开的这个窗口，它是一个顶级的对象**

## 简介

我们平时使用的 `alert、document、setInterval ....` 全部来自于 `window` 对象。（即： `alert === window.alert`）

平时在我们直接触发某个函数时，其实是通过 `window.函数名` 来进行触发的。

# this

## 概念

当前 **函数** 运行时所处的 **环境**

## 简介

通常情况下：**谁调用的函数，`this` 指向谁**



# 节点

## 概念

`html` 中的 **任何一个内容** 都是节点

`html` 标签全部都是节点

## 分类

1. 元素节点
2. 文本节点
3. 属性节点
4. 注释节点

## 增删改查

### 查

```html
<body>
  <div class="cao-cao">
    曹操
    <p class="cao-pi" class="red">曹丕</p>
    <div>
      <!-- 才子 -->
      曹植
    </div>
  </div>
</body>

<script>
  // 节点之间的关系
  // 父节点：曹操的div
  // 子节点：曹丕的p 和 曹植的div
  // 兄弟节点：曹丕和曹植
  const caoCaoEle = document.querySelector('.cao-cao')
  const caoPiEle = document.querySelector('.cao-pi')


  // 需求1： 查。 就是根据节点的关系来进行查找的

  // 1. 父节点查找：根据子节点，找到父节点
  // 1.1 先找到 曹丕
  // 1.2 查找父节点，该父节点是一个元素节点
  console.log(caoPiEle.parentNode)

  // 2. 查找所有子节点：根据父节点，获取所有的子节点
  console.log(caoCaoEle.childNodes);

  // ⭐️ 3. 查找所有元素子节点：根据父节点，只获取所有的 《元素》 子节点
  console.log(caoCaoEle.children);

  // 4. 了解：下一个兄弟节点
  console.log(caoPiEle.nextElementSibling); // 获取下一个兄弟元素节点
  console.log(caoPiEle.nextSibling); // 获取下一个兄弟节点

  // 5. 了解：上一个兄弟节点
  console.log(caoPiEle.previousElementSibling); // 获取上一个兄弟元素节点
  console.log(caoPiEle.previousSibling); // 获取上一个兄弟节点


</script>
```

### 增

```html
<body>
  <div class="cao-cao">
    曹操
    <p class="cao-pi" class="red">曹丕</p>
    <div>
      <!-- 才子 -->
      曹植
    </div>
  </div>
</body>

<script>
  // 节点之间的关系
  // 父节点：曹操的div
  // 子节点：曹丕的p 和 曹植的div
  // 兄弟节点：曹丕和曹植
  const caoCaoEle = document.querySelector('.cao-cao')
  const caoPiEle = document.querySelector('.cao-pi')

  // 需求2：新增一个节点
  // 步骤：
  // 1. 创建节点：创建一个元素节点
  // 创建一个 div 节点
  const caoAngEle = document.createElement('div')
  caoAngEle.innerHTML = '曹昂'
  console.log(caoAngEle);
  // 2. 追加节点：把一个节点放到 html 中的指定位置
  // 2.1：把某一个节点放入到 父节点中的最后
  caoCaoEle.appendChild(caoAngEle)

  // 2.2：把某一个节点放入到 父节点的某一个子节点的前面
  // 父元素.insertBefore(要插入的元素, 在哪个元素的前面)
  caoCaoEle.insertBefore(caoAngEle, caoPiEle)
</script>
```

### 删

```html
<body>
  <div class="cao-cao">
    曹操
    <p class="cao-pi" class="red">
      曹丕
      <span class="cao-rui">曹睿</span>
    </p>
    <div>
      <!-- 才子 -->
      曹植
    </div>
  </div>
</body>

<script>
  // 节点之间的关系
  // 父节点：曹操的div
  // 子节点：曹丕的p 和 曹植的div
  // 兄弟节点：曹丕和曹植
  const caoCaoEle = document.querySelector('.cao-cao')
  const caoPiEle = document.querySelector('.cao-pi')
  const caoRuiEle = document.querySelector('.cao-rui')

  // 删除节点：
  // 父元素.removeChild(要删除的子元素)  从父元素中删除指定的子元素
  // 注意：该方法只能由父元素发起，删除对应的子元素
  caoCaoEle.removeChild(caoPiEle)
  // caoCaoEle.removeChild(caoRuiEle) // 不可以的，删除子节点只能是亲儿子
</script>
```

### 重点需要记住的

```js
// 1. 如何创建一个元素节点
document.createElement('标签名')

// 2. 如何把一个节点插入到另外一个节点中
父元素.appendChild(子节点)
父元素.insertBefore(插入的节点, 在当前节点之前)

// 3. 删除节点和 display:none 有区别吗？区别是什么？
删除节点：会直接把节点从 节点树 之中删除，该节点就没有（ G 了）
display:none：把节点隐藏。 该节点依然存在，只是看不到了

// 4. 如何获取所有的元素子节点
父节点.children
```



# 本地存储

浏览器提供了两个把数据保存到本地的 API：



window.sessionStorage: 把数据保存到浏览器中。注意：当关闭 table 页 的时候，保存的数据会消失

`window.sessionStorage.setItem('key', 'value')`

保存：`setItem(key, value)`

获取：`getItem(key)`

删除指定key的数据： `removeItem(key)`

清空所有保存的数据：`clear()`

```js
const name = '张三'
window.sessionStorage.setItem('name', name)
```



window.localStorage：与 sessionStorage 一样（所有的方法），把数据保存到浏览器中。但是，他与 sessionStorage 唯一不同的是：它保存在浏览器中的数据，永远都不会主动消失。如果你想要删除这个数据，那么必须通过  removeItem 或者 clear 来去实现

`window.localStorage.setItem('key', 'value')`

保存：setItem(key, value)

获取：`getItem(key)`

删除指定key的数据： `removeItem(key)`

清空所有保存的数据：`clear()`



### 简单数据类型

简单数据类型而言，保存是不需要转化的，直接保存即可。

### 复杂数据类型

复杂数据类型，通常指的就是：**对象** 或者 **数组**。

但是对于 `localStoreage` 而言，它不能直接保存一个复杂数据类型。所以说，我们想要存储复杂数据类型的话，那么需要：**把复杂数据类型，转化为 字符串 ，进行保存**

那么怎么转化呢？ 此时需要使用到 `JSON.stringify(obj)`：该方法可以**把一个对象，字符串化（`json 格式的字符串`）**。

转化为字符串之后，就可以保存了。

因为保存到 `localStorage` 中的是一个字符串，所以取出来的数据也会是字符串。那么此时该数据就不能当成对象使用了。

所以此时，我们需要利用另外一个方法：`JSON.parse(str)`。该方法可以把一个 **字符串转化为对象**。但是要注意，这个字符串必须是 `json 格式的字符串`
