// 1. 如何创建一个元素节点
document.createElement('标签名')

// 2. 如何把一个节点插入到另外一个节点中
父元素.appendChild(子节点)
父元素.insertBefore(插入的节点, 在当前节点之前)

// 3. 删除节点和 display:none 有区别吗？区别是什么？
// 删除节点：会直接把节点从 节点树 之中删除，该节点就没有（ G 了）
// display:none：把节点隐藏。 该节点依然存在，只是看不到了

// 4. 如何获取所有的元素子节点
父节点.children
